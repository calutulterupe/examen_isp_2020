package Boboc_Bogdan_Ioan;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Subiect2 {
    public static void main(String[] args) {

        ButtonAndTextField interfata=new ButtonAndTextField();

    }
}
class ButtonAndTextField extends JFrame {

    JLabel lbl1,lbl2;
    JTextField txtField1,txtField2;
    JButton bLoghin;

    ButtonAndTextField(){
        
        setTitle("Examen");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,250);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=80;int height = 20;

        lbl1 = new JLabel("Text1 ");
        lbl1.setBounds(10, 50, width, height);

        lbl2 = new JLabel("Text2 ");
        lbl2.setBounds(10, 100,width, height);

        txtField1 = new JTextField();
        txtField1.setBounds(70,50,width, height);

        txtField2 = new JTextField();
        txtField2.setBounds(70,100,width, height);

        bLoghin = new JButton("Replace");
        bLoghin.setBounds(10,150,width, height);
        ButtonAndTextField eu=this;
        bLoghin.addMouseListener(this.giveMeAMouse());

        add(lbl1);add(lbl2);add(txtField1);add(txtField2);add(bLoghin);

    }
    private MouseListener giveMeAMouse()
    {
        return new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                txtField2.setText(txtField1.getText());


            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

        };
    }


}