package Boboc_Bogdan_Ioan;

import java.util.Random;

public class Subiect1
{
    public static void main(String[] args) {
        B objectB=new B();
        objectB.b();
        C objectC=new C();
        objectB.doSomethingWithC(objectC);
        D objectD=new D();
        objectD.met1(0);
        F objectF=new F();
        E objectE=new E();
        objectF.n("acest tring este la baza");
        objectE.met1();
        objectD.setObjectF(objectF);
        G objectG=new G();
        objectG.met3();
        objectD.setOpbjectG(objectG);


    }
}
abstract class A{
    public abstract void b();
}
class B extends A{
    public B()
    {
        System.out.print("You just initialized a B class");
    }
 private long t;
 private D objectD;
 @Override
 public void b()
 {
     objectD=new D();
     System.out.println();
     System.out.println("You have called B.b()");

 }
 public void doSomethingWithC(C c)
 {
     System.out.print("Did something with C");
 }


}
class D{
    public D()
    {
        System.out.print("You just initialized a D class");
    }
    public G opbjectG;
    public F objectF;

    public void met1(int i)
    {
        System.out.println("You have called D.met1(int i) with i="+i);
        E objectE=new E();
        objectE.met1();
    }

    public void setObjectF(F objectF) {
        this.objectF = objectF;
    }

    public void setOpbjectG(G opbjectG) {
        this.opbjectG = opbjectG;
    }

}
class G{
    public G()
    {
        System.out.print("You just initialized a G class");
    }
    public double met3()
    {
        System.out.println("You have called G.met3()");
       Random random=new Random();
       return random.nextDouble();
    }
}
class F{
    public F()
    {
        System.out.print("You just initialized a F class");
    }
    public void n(String s)
    {
        System.out.println("You have called F.n()");
    }

}
class E{
    public E()
    {
        System.out.print("You just initialized a E class");
    }
    public void met1()
    {
        System.out.println("You have called E.met1");
    }

}
class C{
    public C()
    {
        System.out.print("You just initialized a C class");
    }
}